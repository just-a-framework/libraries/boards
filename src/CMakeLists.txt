cmake_minimum_required(VERSION 3.12)

enable_testing()

if(CONAN_BUILD)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)

    set(PACKAGE_NAME ${CONAN_PACKAGE_NAME})
    set(PACKAGE_VERSION ${CONAN_PACKAGE_VERSION})
else()
    set(PACKAGE_NAME "boards")
    set(PACKAGE_VERSION "0.0.0")
endif()

set(target_name ${PACKAGE_NAME})

project(${PACKAGE_NAME}
    LANGUAGES CXX
    VERSION ${PACKAGE_VERSION}
)

if(CONAN_BUILD)
    conan_basic_setup()
endif()

set(include_dir ${CMAKE_CURRENT_SOURCE_DIR}/include/${target_name})

set(HEADERS
    ${include_dir}/empty.hpp
)

set(SOURCES
    empty.cpp
)

add_library(${target_name}
	${SOURCES}
	${HEADERS}
)

set_target_properties(${target_name}
	PROPERTIES OUTPUT_NAME "${target_name}-${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}"
)

target_compile_features(${target_name}
	PUBLIC
		cxx_std_17
)

target_include_directories(${target_name}
	PRIVATE
		include
)

target_include_directories(${target_name}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  		$<INSTALL_INTERFACE:include>
)

add_library(jaf::${target_name} ALIAS ${target_name})

include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

set(INSTALL_RUNTIME_DIR ${CMAKE_INSTALL_BINDIR})
set(INSTALL_CONFIG_DIR  ${CMAKE_INSTALL_LIBDIR}/jaf/cmake)
set(INSTALL_LIBRARY_DIR ${CMAKE_INSTALL_LIBDIR}/jaf)
set(INSTALL_ARCHIVE_DIR ${CMAKE_INSTALL_LIBDIR}/jaf)
set(INSTALL_INCLUDE_DIR ${CMAKE_INSTALL_INCLUDEDIR})

set(PROJECT_CONFIG_VERSION_FILE "${CMAKE_BINARY_DIR}/BoardsConfigVersion.cmake")
set(PROJECT_CONFIG_FILE         "${CMAKE_BINARY_DIR}/BoardsConfig.cmake")

configure_package_config_file(cmake/BoardsConfig.cmake.in
    ${PROJECT_CONFIG_FILE}
    INSTALL_DESTINATION ${INSTALL_CONFIG_DIR}
)

write_basic_package_version_file(
    ${PROJECT_CONFIG_VERSION_FILE}
    COMPATIBILITY SameMajorVersion
)

install(TARGETS ${target_name}
    EXPORT ${target_name}-targets
    RUNTIME DESTINATION ${INSTALL_RUNTIME_DIR}
    LIBRARY DESTINATION ${INSTALL_LIBRARY_DIR}
    ARCHIVE DESTINATION ${INSTALL_ARCHIVE_DIR}
)

install(DIRECTORY include/
    DESTINATION ${INSTALL_INCLUDE_DIR}
)

install(FILES
    ${PROJECT_CONFIG_VERSION_FILE}
    ${PROJECT_CONFIG_FILE}
    DESTINATION ${INSTALL_CONFIG_DIR}
)

install(EXPORT ${target_name}-targets
    FILE BoardsTargets.cmake
    NAMESPACE jaf::
    DESTINATION ${INSTALL_CONFIG_DIR}
)

export(EXPORT ${target_name}-targets
    FILE ${CMAKE_CURRENT_BINARY_DIR}/BoardsTargets.cmake
    NAMESPACE jaf::
)

export(PACKAGE ${target_name})
