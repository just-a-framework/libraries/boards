from conans import ConanFile, CMake

class Boards(ConanFile):
    name = "boards"
    version = "0.0.0"
    description = "C++17 compatible smb wrapper."
    url = "https://gitlab.com/just-a-framework/libraries/boards"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "src*"
    build_requires = "cmake_installer/3.16.2@conan/stable"

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CONAN_BUILD"] = "ON"
        cmake.configure(source_folder="src")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.builddirs = ["lib/jaf/cmake"]
